Source: starjava-registry
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: java
Priority: optional
Build-Depends: ant, debhelper-compat (= 12), dh-exec, javahelper
Build-Depends-Indep: default-jdk, default-jdk-doc, javacc
Standards-Version: 4.4.0
Vcs-Browser: https://salsa.debian.org/debian-astro-team/starjava-registry
Vcs-Git: https://salsa.debian.org/debian-astro-team/starjava-registry.git
Homepage: https://github.com/Starlink/starjava/tree/master/registry

Package: starlink-registry-java
Architecture: all
Depends: ${java:Depends}, ${misc:Depends}
Description: Starlink IVOA registry access
 This package provides a set of classes which can be used to make
 lightweight queries of an RI 1.0-compliant IVOA registry. In the
 Virtual Observatory (VO), registries provide a means for discovering
 useful resources, i.e., data and services. Individual publishers
 offer the descriptions for their resources ("resource records") in
 publishing registries.
 .
 This is not an all-singing or all-dancing registry client. The focus
 is on something which does a limited job fast and in a small memory
 footprint.

Package: starlink-registry-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Recommends: ${java:Recommends}
Description: Starlink IVOA registry access (API docs)
 This package provides a set of classes which can be used to make
 lightweight queries of an RI 1.0-compliant IVOA registry. In the
 Virtual Observatory (VO), registries provide a means for discovering
 useful resources, i.e., data and services. Individual publishers
 offer the descriptions for their resources ("resource records") in
 publishing registries.
 .
 This package contains the JavaDoc documentation of the package.
